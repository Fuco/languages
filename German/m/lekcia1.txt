gut; dobrý, dobre
Guten Tag; dobrý deň
mein, meine; môj, moje, moja
Meine Damen und Herren; Dámi a páni
herzlich; srdečne
willkommen zu...; vítame vás na...
Herzlichen willkommen!; Srdečne vás vítame
unser, unsere; náš, naše, naša
zu unserem Deutschkurs; v našom kurze nemčiny
Mein Name ist; Moje meno je/Volám sa
ist; je
und; a
das sind; to sú
unsere Freunde; naši priatelia
sie; ona
jung; mladý, mladá
blond; svetlovlasý, -á, -é
klein; malý, -á
hübsch; pekný, -á, -é
er; on
auch; aj, tiež
ziemlich; dosť
groß; veľký/vysoký, -á, -é
schlank; štíhly, -a, -e
Sie; vy (vykanie)
Ja.; Áno
ich bin; (ja) som
woher; odkiaľ
kommen; pochádzať
Woher kommen Sie?; Odkiaľ pochádzate?
aus Berlin; z Berlína
..., nicht wahr?; ..., však/že?
vorstellen; predstaviť
Darf ich vorstellen?; Dovoľte, aby som vás predstavil.
Freut mich.; Teší ma.
was; čo
Wir machen ein Interview; Urobíme rozhovor/interview
jetzt; teraz
mit Ihnen; s vami (vykanie)
wie; ako
heißen; volať sa
bitte; prosím
Wie bitte?; Ako prosím?
du; ty
wo; kde
wohnen; bývať
noch; ešte
immer noch; ešte stále
in; v
Nein; Nie.
Was sind Sie von Beruf?; Aké máte povolanie?
Ich bin Dolmetscherin.; Som tlmočníčka.
verstehen; rozumieť
nicht; ne- (zápor)
Danke für...; Ďakujem za...
also; teda
alles; všetko
heute; dnes
für heute; na dnes/pre dnes
Auf Wiedersehen!; Dovidenia!
Tschüs!; Ahoj! (vale, pri lúčení)
sein; byť
es; ono, to
ihr; vy
sie; oni
der, die, das; ten, tá, to (určitý člen)
ein, eine, ein; jeden, jedna, jedno/nejaký, -á, -é (neurčitý člen)
wer; kto
das Österreich; Rakúsko
Wien; Viedeň
die Stadt, ä-e; mesto
Haupt-; hlavné ...
die Hauptstadt, ä-e; hlavné mesto
der Kaufmann, ä-er; obchodník
arbeiten; pracovať
die Bank, -en; banka
in einer Bank; v banke
das Tschechien; Česko
Prag; Praha
der Student, -en; študent
die Studentin, -nen; študentka
studieren; študovať (v)
die Soziologie; sociológia
die Universität, -en; univerzita
an der Universität; na univerzite
leben; žiť
sein, seine; jeho
die Familie, -n; rodina
mit seiner Familie; so svojou rodinou
arbeiten als; pracovať ako
der Sport; šport
der Lehrer, -; učiteľ
das Gymnasium, -en; gymnázium
am Gymnasium; na gymnáziu
die Deutsche, -n; Nemka
bei; pri, u
die Kosmetikerin, -nen; kozmetička
das Land, ä-er; krajina
der Einwohner, -; obyvaťel
die Sprache, -n; jayzk, reč
sprechen; hovoriť
sehr (gut); veľmi (dobre)
ganz (gut); celkom (dobre)
Es geht.; Ujde to.
nicht so gut; nie moc dobre
nicht besonders; nie obzvlášť dobre
schlecht; zle
das Auto, -s; auto
