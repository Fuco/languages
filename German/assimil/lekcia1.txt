Erste Lektion - Im Café

1 - Herr Ober!                            1 - Pán čašník!
2   Der Tee ist kalt!                     2   Čaj je studený!
3 - Wie ist der Tee?                      3 - Aký je čaj?
4 - Er ist kalt!                          4 - Je studený.
5 - Oh, Verzeihung!                       5 - Oh, prepáčte!
6 - Herr Ober, der Tee ist jetzt gut;     6 - Pán čašník, čaj je už dobrý;
7   aber die Tasse...                     7   ale šálka...
8 - Ja, die Tasse?                        8 - Áno, šálka?
9 - Sie ist zu klein!                     9 - Tá je moc malá!

Übung 1 - Übersetzen Sie bitte

1. Wie ist die Tasse?
2. Sie ist klein.
3. Wie ist der Tee?
4. Er ist zu kalt.
5. Peter ist klein, aber Klaus ist groß.

Übung 2 - Ergänzen Sie bitte

1. ... ist der Tee?                       1. Aký je čaj?
2. .. ist gut.                            2. Je dobrý.
3. .......... Herr Ober!                  3. Prepáčte, pán vrchný!
4. Die Tasse ist .. klein.                4. Šálka je príliž malá.
5. ... ist klein.                         5. Ona je malá.
6. Anne ist ....., aber Klaus ist groß.   6. Anna je malá, ale Klaus je velký (vysoký).















Die Lösung
Ü1: 1. Aká je šálka? 2. Je malá. 3. Aký je čaj? 4. Je príliž chladný. 5. Peter je malý, ale Klaus je vysoký.
Ü2: 1. Wie 2. Er 3. Verzeihung 4. zu 5. Sie 6. klein
