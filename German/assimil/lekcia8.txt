 Achte Lektion - Ein Fest (n.) [= party]

 1   Viele Leute sind heute abend bei    1   Veľa ľudí je dnes večer u
     Fischers.                               Fisherovcou.
 2   Fischers geben eine Party.          2   Fisherovci majú (dávajú) párty.
 3   Man trinkt, tanzt und lacht viel.   3   Pije sa, tancuje sa a veľa sa smeje.
 4   Alle amüsieren sich gut. Alle?      4   Všetci sa bavia. Všetci?
 5   Wer ist die Frau dort? Sie ist      5   Kdo je tá žena tam? Je celkom sama.
     ganz allein.
 6   Ich möchte wissen (1), wer sie ist. 6   Chcel by som vedieť, kdo (ona) je.
 7 - Anne, wer ist die blonde (2) Frau   7 - Anne, kdo je tá blond žena tam?
     dort?
 8 - Ich weiß nicht. Ich kenne sie nicht. 8 - Neviem. Nepoznám ju.
 9   Aber ich glaube, sie ist eine       9   Ale myslím si (verím), [že] je
     Freundin (3) von Frau Fischer.          priateľka pani Fisherovej.
10 - Gut! Ich frage                     10 - Dobre! Opýtam sa jej...
     sie... (Fortsetzung folgt)              (pokračovanie nasleduje)

(1) wissen: ich weiß, du weißt, er weiß, wir wissen, Sie wissen
(2) prídavné meno po určitom člene končí v 1. páde -e: die blondE Frau, der altE Mann, das kleinE Kind.
(3) der Freund, die FreundIN... der Student, die StudentIN...

Übung 1 - Übersetzen Sie bitte

1. Wer ist die Freundin von Frau Fischer?
2. Die Leute trinken und lachen.
3. Das Kind is ganz allein.
4. Kennen Sie Fräulein Wagner?
5. Der kleine Mann dort ist mein Freund.

Übung 2 - Ergänzen Sie bitte

1. ... ist das? - Ich .... nicht.       1. Kdo je to? - Neviem.
2. .... amüsieren sich und ...... .     2. Všetci sa bavia a tancujú.
3. Ich ...... er ist ein ...... von     3. Myslím (verím), že je priateľom
   Fräulein Schmitt.                       slečny Schmitt.
4. Er ..... sie nicth.                  4. [on] Nepozná ju.
5. Wissen Sie, ..... er lacht?          5. Viete, prečo sa [on] smeje?
6. Die ...... Frau dort ist meine       6. Tá malá žena tam je moja mama.
   Mutter.

Die Lösung
Ü1: 1. Kdo je priateľka pani Fisher? 2. Ľudia pijú a smejú sa. 3. Dieťa je celkom samé. 4. Poznáte slečnu Wagner? 5. Ten malý muž tam je moj priateľ.
Ü2: 1. Wer - weiß 2. Alle - tanzen 3. glaube - Freund 4. kenne 5. warum 6. kleine
