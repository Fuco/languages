Zwanzigste Lektion - Wo ist der Bahnhof?

 1 - Weißt du, wo der Bahnhof ist?       1 - Vieš, kde je stanica?
 2 - Keine Ahnung (1). Wir müssen        2 - Nemám potuchy. Musíme sa opýtať.
     fragen.
 3   Entschuldigen Sie bitte, wo...      3   Prepáčte prosím, kde...
 4 - Die Leute haben alle keine Zeit.    4 - Ľudia [ne]majú (všetci) (žiaden)
                                             čas.
 5 - Warte, ich habe eine Idee. Dort     5 - Počkaj, mám nápad. Tam je
     ist ein Hotel. Ich bin gleich           hotel. Hned som naspäť.
     zurück.
 6 - Guten Abend! Haben Sie ein Zimmer   6 - Dobrý večer! Máte volnú izbu?
     (2) frei?
 7 - Sicherlich, mein Herr. Möchten      7 - Samozrejme pane. Chceli by ste
     Sie ein Doppelzimmer oder ein           dvojizbu(lôžko) alebo jednoizbu?
     Einzelzimmer?
 8 - Ein Zimmer für sechs Personen,      8 - Izbu pre šesť osôb, prosím.
     bitte.
 9 - Wiie bitte? Wie viele Personen?     9 - Ako prosím? Koľko osôb? Šesť
     Sechs Personen?                         osôb?
10   Dann nehmen Sie doch gleich einen  10   Tak to si potom vezmite rovno
     Liegewagen. Dort haben Sie sechs        lôžkový vagón. Tam majú šesť
     Plätze.                                 miest.
11 - Ach, ja. Das ist eine gute         11 - Ah. To je dobrý nápad. Môžete mi
     Idee. Können Sie mir bitte sagen,       povedať, kde je stanica?
     wo der Bahnhof ist?
12 - Sie fahren die erste Straße links  12 - Pôjdete na prvej ulici doľava,
     und dan die zweite rechts, und          potom na druhej doprava a uvidíte
     Sie sehen den Bahnhof gleich            stanicu rovno pred sebou.
     gegenüber.
13 - Danke schön! Auf Wiedersehen!      13 - Dakujem pekne! Dovidenia!

(1) die Ahnung = nápad, idea, potucha. Všetky slová na -ung sú ženského rodu a množné číslo je -ungen
(2) das Zimmer = izba. Slová na -er sa nemenia v množnom čísle (das Zimmer -> die Zimmer). Podobne slová na -en.

Übung 1 - Übersetzen Sie bitte

1. Wollen Sie ein Doppelzimmer oder ein Einzelzimmer?
2. Ein Zimmer für zwei Personen, bitte.
3. Ich weiß nicht, wo meine Tante wohnt.
4. Sie weiß nicht, wo ihre Tante wohtn.
5. Wie viele Leute wohnen hier?
6. Wir müssen fragen, wieviel Uhr es ist.

Übung 2 - Ergänzen Sie bitte

1. ............. Sie, wissen Sie       1. Prepáčte, viete koľko je hodín?
   ....... Uhr es ist?
2. Ich möchte bitte ein                2. Chcel by som izbu pre dvoch na
   ............ für eine Nacht.           jednu noc.
3. ...... Sie mir sagen, .. ein Hotel  3. Môžete mi povedať, kde je (nejaký)
   ist?                                   hotel?
4. Die erste Straße ..... und dan      4. Na prvej ulici dolava a potom stále
   ..... geradeaus.                       rovno.
5. Guten .....! Haben Sie ein Zimmer   5. Dobrý večer! Máte volnú izbu?
   ....?
6. ...... Sie! Wir kommen ......!      6. Počkajte! Hneď (Už aj) ideme!

Die Lösung
Ü1: 1. Chcete dvojizbu alebo jednoizbu? 2. Izbu pre dve osoby, prosím. 3. Neviem, kde býva moja teta. 4. Ona nevie, kde býva jej teta. 5. Koľko ludí tu býva? 6. Musíme sa opýtat, koľko je hodín.
Ü2: 1. Entschuldigen - wieviel 2. Doppelzimmer 3. können - wo 4. links - immer 5. Abend - frei 6. warten - gleich (sofort)
