 Fünfte Lektion - Am Telefon (n.)

 1 - Guten Tag! Hier ist Peter           1 - Dobrý ďeň! Tu je Peter Schmitt.
     Schmitt.
 2   Ich möchte (1) bitte [mit]          2   Chcel by som [hovoriť] so slečnou
     Fräulein Wagner sprechen.               Wagner hovoriť.
 3 - Verzeihung! Wer sind Sie?           3 - Prepáčte! Kto je tam? (Kto ste?)
 4 - Mein Name ist Peter Schmitt.        4 - Moje meno (m.) je Peter Schmitt.
 5 - Einen Moment, bitte. Meine          5 - Moment (m.), prosím. Moja dcéra
     Tochter kommt sofort.                   [hneď] príde hneď.
 6 - Hallo, Peter! Wo bist du?           6 - Čau, Peter! Kde si?
 7 - Ich bin noch im Büro; aber ich      7 - Som ešte v práci (úrade (m.)); ale už
     fahre jetzt nach Hause (2).             (teraz) odchádzam (jazdím) domov.
 8   Gehen wir heute abend ins Kino?     8   Ideme dnes večer do kina (n.)?
 9 - Nein, lieber morgen; heute abend    9 - Nie, radšej zajtra; dnes večer by
     möchte ich fernsehen.                   som chcela pozerať televízor.
10 - Gut! Dann bis morgen!              10 - Dobre! Tak (potom) do zajtra!
11   Ich bin, du bist, er/sie/es ist,   11   Ja som, ty si, on/ona/ono je, Vy
     Sie sind.                               ste

(1) slovosled: Ich möchte (vyčasované) ... sprechen (neurčitok).
(2) das Haus = dom, nach Hause (gehen) = (ísť) domov, Ich bin zu Hause = Som doma. 'nach' vyjadruje smer na miesto.

Übung 1 - Übersetzen Sie bitte

1. Wer sind Sie? - Ich bin Anne Müller.
2. Herr Schmitt geht nach Hause.
3. Fräulein Wagner ist noch im Büro.
4. Gehst du heute abend ins Restaurant?
5. Herr und Frau Herder sind sehr müde.
6. Kommen Sie mit ins Kino?

Übung 2 - Ergänzen Sie bitte

1. Guten Tag! .... ist Fräulein Wagner. 1. Dobrý deň! Tu je slečna Wagner.
2. Mein .... ist Wolfgang.              2. Moje meno je Wolfgang.
3. Verzeihung, ... bist du?             3. Prepáčte, kto je tam? (kto si?)
4. Ich gehe jetzt .... Hause.           4. Idem už (teraz) domov.
5. .. ist Peter? - Er ist .. Hause.     5. Kde je Peter? - Je doma.
6. Meine ....... ist noch klein.        6. Moja dcéra je ešte malá.

Die Lösung
Ü1: 1. Kto ste? Som (Tu je) Anna Müller 2. Pán Schmitt ide domov. 3. Slečna Wagner je ešte v práci. 4. Ideš dnes večer do reštaurácie? 5. Pán a pani Herder sú veľmi (strašne) unavený. 6. Idete (Pojdete) s nami/so mnou do kina?
Ü2: 1. Hier 2. Name 3. wer 4. nach 5. Wo - zu 6. Tochter
