http://www.virdrinksbeer.com/page29.htm

Milites agricolam rogaverunt cur uxorem e foro traheret.
The soldiers asked the farmer why he was dragging his wife from the forum.

Dic mihi ubi gladium meum posueris!
Tell me where have you put my sword!

Scisne cur frater tuus saxa ad illum canem iaciat?
Do you know why is your brother throwing rocks at that dog?

Nescio num cras adventurus sim.
I don't know if I will come/arrive tomorrow.

Mater irata audivit unde filia dona accepisset.
Furious mother heard from where the daughter had recieved the gifts.

Brevi tempore omnes sciemus quis hanc puellam pulchram necaverit.
In a short while we will all know who has killed this beautiful girl.

Dux cognoscere conabatur quantae essent copiae hostium.
The general was trying to get to know what was the extend of enemy's army.

Milites a iuvene quaerebant quis esset et quo iret.
The soldiers were asking a young man who he was and where he was going.

Omnes scimus qualis homo sis, Marce.
We all know what kind of a man you are, Marcus.

Heri cognovi unde parentes hanc mensam emerint.
I learned/found out yesterday where parents bought this table.

Amici se rogabant quando iter longum confecturi essent.
The friends were asking when they would finish the long journey.

Nonne scis quot ancillas habeas!
You don't know how many maids you have, do you!

------------------------

The soldiers asked the poet why he was lying in the middle of the road.
Milites poetam rogaverunt (a poeta quaesiverunt) cur in media via iaceret.

The Emperor wants to find out who (his) enemies are.
Imperator invenire/cognoscere vult qui hostes sui sint.

The master asked the slave why he was holding (his) shield.
Dominus servum rogavit cur scutum teneret.

The inhabitants didn't know when (their) city had been built.
Incolae nesciverunt/nesciebant [which one?] quando urbs aedificata esset.

Ask your son if he has had enough dinner.
Roga filium tuum num iam satis cenaverit.

I don't know how we will ever escape from this danger.
Nescio quomodo umquam ex hoc periculo effugituri simus.

Don't ask me what you ought to do!
Noli me rogare quid facere debeas!

Didn't you know when the ship was going to set off? (use Proficiscor)
Nonne sciebas quando navis profectura esset?
