http://www.virdrinksbeer.com/page28.htm

Septimus rex erat tam crudelis ut superbus a Romanis vocaretur.
The seventh king was so cruel that he was called "???" by Romans. (that the Romans called him "???")

Pompeios totiens ivi ut urbem iam optime cognoverim.
I have been to Popmey so many times that I already know the city very well. [cognovi = novi? perf->pres]

Frater meus tantos pedes habet ut caligae sibi inveniri non possint.
My brother has such (great) feet that he can't find boots for himself.

Toga tua tam vetus est ut novam emere vero debeas.
Your toga is so old that you should really buy a new one.

Milites tantopere in via clamabant ut mulieres terrerentur.
The soldiers were shouting in the street so much that they scared the wifes. (that the wifes were scared)

Senex pecuniam ita celaverat ut a filiis non inventa esset.
The old men had _so_ hidden the money that it had not been found by his sons.

Villa nostra tam longe Roma abest ut uno die advenire non possitis.
Our mansion is so far from Rome that you will not be able to reach it in one day.

Dominus erat tam iratus ut servum in flumen iecisset.
(The) Master was so angry that he had thrown the slave into the river.

Mater liberorum adeo gaudebat ut loqui non posset.
The mother of children was so happy that she couldn't speak.

Iuvenes talia verba clamabant ut puellae audire nollent.
The young (men/boys) were shouting such words that the girls didn't want to listen.

Urbs tam bene defensa erat ut numquam oppugnata esset.
The city was so well defended that it had never been conquered.

Sunt tot arbores ut silvam videre non possim.
There are so many trees that I can't see the forest.

---------------

The river was so fast that the woman could not cross.
Flumen tam celere erat ut femina transire non posset.

The boys had worked with such great care that the master praised them. [simple past]
Pueri tanta cura/diligentia laboraverant ut magister eos laudaverit.

The poor girl was weeping so much that everyone was looking at her. [cont]
Puella misera tantopere plorabat ut omnes eam spectarent.

The soldiers had pitched camp in such a way that the enemy hadn't been able to get in.
Milites castra tale modo erexerant ut hostes non intrare potuissent.

There were so many arrows that I couldn't see the sky.
Tantae/tot sagittae fuerunt ut caelum videre non potuerim.
Tantae/tot sagittae erant ut caelum videre non possem.
[rozdil?]

My brother has drunk so much wine that he cannot walk.
Frater meus tantum vini bibit ut ambulare non possit.

That merchant has so many slaves that he doesn't know all (their) names.
Mercator ille tantos servos habet ut omnia nomina eorum non noverit. [nesciat?]

This work is so easy that you have finished already.
Hoc opus tam facile est ut iam perfeceris.
