- if the verb root ends in -ā, the perfect ending is -au (instead of -a)
  - √dā -> datau

* √vac
** √vac (speak) 2P Perfect
उवाच | ऊचतुः| ऊचुः
uvāca | ūcatuḥ | ūcuḥ
uvāc+a | ūc+atus | ūc+us
उवक्थ | ऊचथुः | ऊच
uvaktha | ūcathuḥ | ūca
uvac+tha | ūc+athus | ūc+a
उवच | ऊचिव | ऊचिम
uvaca | ūciva | ūcima
uvac+a | ūc+i+va | ūc+i+ma

- singular stem is strong
- *i* is inserted before va/ma

** √vac (speak) 2P Perfect middle
ऊचे | ऊचाते | ऊचिरे
ūce | ūcāte | ūcire
ūc+e | ūc+āte | ūc+ire
ऊचिषे | ऊचाथे | ऊचिध्वे
ūciṣe | ūcāthe | ūcidhve
ūc+i+se | ūc+āthe | ūc+i+dhve
ऊचे | ऊचिवहे | ऊचिमहे
ūce | ūcivahe | ūcimahe
ūc+e | ūc+i+vahe | ūc+i+mahe

- all forms have weak stem (like pres.ind.mid)
* √as
** √as 2P (be)Perfect
आस | आसतुः | आसुः
ās+a | ās+atus | ās+us
आसिथ | आसथुः | आस
ās+i+tha | ās+athus | ās+a
आस | आसिव | आसिम
ās+a | ās+i+va | ās+i+ma
