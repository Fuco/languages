(TeX-add-style-hook
 "verb-pres-act-imp"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("memoir" "11pt" "article" "oneside")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "american" "english") ("csquotes" "babel") ("hyperref" "xetex") ("ucharclasses" "Devanagari")))
   (TeX-run-style-hooks
    "latex2e"
    "memoir"
    "memoir11"
    "fontspec"
    "xunicode"
    "url"
    "rotating"
    "babel"
    "csquotes"
    "soul"
    "hyperref"
    "graphicx"
    "longtable"
    "float"
    "ucharclasses")))

